package com.cleverlance.bootcamp.common;

public class Constants {

    // =============================================================================
    // Animations
    // =============================================================================

    public static int FADEIN_ANIMATION_LENGTH = 700;
    public static int FADEOUT_ANIMATION_LENGTH = 700;
    public static int TRANSLATE_ANIMATION_LENGTH = 1000;

    // =============================================================================
    // JSON attributes
    // =============================================================================

    public static String IMAGE = "image";

}
