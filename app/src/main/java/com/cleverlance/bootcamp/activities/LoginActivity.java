package com.cleverlance.bootcamp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cleverlance.bootcamp.common.Constants;
import com.cleverlance.bootcamp.common.Enums;
import com.cleverlance.bootcamp.utility.HttpPostHandler;
import com.cleverlance.bootcamp.utility.OnHttpPostResponse;
import com.cleverlance.bootcamp.utility.SHA1Converter;

import cleverlance.com.cleverlancebootcamp.R;

public class LoginActivity extends Activity implements OnHttpPostResponse{

    // =============================================================================
    // Fields
    // =============================================================================

    String username = null;
    String encryptedPassword = null;
    ProgressBar progressBar;
    Button downloadButton;
    ImageView iw;
    RelativeLayout loginLayout;

    Animation fadeIn;
    Animation fadeOut;
    TranslateAnimation translateAnimation;

    // =============================================================================
    // Override methods
    // =============================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.progressBar = (ProgressBar) findViewById(R.id.login_progressBar);
        this.downloadButton = (Button) findViewById(R.id.login_button);
        this.iw = (ImageView) findViewById(R.id.login_imageView);
        this.loginLayout = (RelativeLayout) findViewById(R.id.login_layout);
        createFadeAnimations();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // changing the orientation makes the login Relative Layout pop up to the top
        executeTranslateAnimation();
    }

    @Override
    public void onHttpPostResponse(Bitmap bitmap, Enums.postResponseStatus status) {
        downloadButton.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        if (status == Enums.postResponseStatus.WRONG_CREDENTIALS){
            Toast.makeText(this, getString(R.string.wrongCredentials), Toast.LENGTH_SHORT).show();
        }
        else if (status == Enums.postResponseStatus.CONNECTION_ERROR){
            Toast.makeText(this, getString(R.string.connectionProblem), Toast.LENGTH_SHORT).show();
        }
        else if (status == Enums.postResponseStatus.OK){
            iw.setImageBitmap(bitmap);
            iw.startAnimation(fadeIn);
            downloadButton.startAnimation(fadeIn);
        }
    }

    // =============================================================================
    // Methods
    // =============================================================================

    /**
     * Checks if the device is online and processes the input from EditTexts
     * Also animates the UI elements
     * @param button
     */
    public void download_onClick(View button){
        if (isDeviceOnline()){
            executeTranslateAnimation();
            button.setVisibility(View.INVISIBLE);
            this.progressBar.setVisibility(View.VISIBLE);

            if (iw.getDrawable() != null) {
                iw.startAnimation(fadeOut);
            }
            this.processInput();
        }
        else {
            Toast.makeText(this, getString(R.string.offlineMessage), Toast.LENGTH_SHORT).show();
            button.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    /**
     *  Processes the data from EditTexts and hashes the password (SHA-1)
     */
    private void processInput(){
        EditText usernameEditText = (EditText) findViewById(R.id.login_username);
        EditText passwordEditText = (EditText) findViewById(R.id.login_password);

        this.username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        this.encryptedPassword = SHA1Converter.toSHA1(password.getBytes());
        executeHttpRequest();
    }

    private void executeHttpRequest(){
        HttpPostHandler handler = new HttpPostHandler(this, username,encryptedPassword);
        handler.execute();
    }


    /**
     * Creates basic fade-in and fade-out animations
     */
    private void createFadeAnimations(){
        fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(Constants.FADEIN_ANIMATION_LENGTH);

        this.fadeOut = new AlphaAnimation(1, 0);
        this.fadeOut.setInterpolator(new AccelerateInterpolator());
        this.fadeOut.setDuration(Constants.FADEOUT_ANIMATION_LENGTH);
        this.fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                iw.setImageBitmap(null);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    /**
     * Executes the translate animation of the input layout from its position to the top
     */
    private void executeTranslateAnimation(){
        translateAnimation = new TranslateAnimation(0f, 0f, 0f, 0-loginLayout.getY());
        translateAnimation.setDuration(Constants.TRANSLATE_ANIMATION_LENGTH);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                loginLayout.clearAnimation();
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0,0,0,0);
                loginLayout.setLayoutParams(params);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        loginLayout.setAnimation(translateAnimation);
    }

    /**
     * Method that checks if the device is connected to Internet
     * @return true/false
     */
    public boolean isDeviceOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    // =============================================================================
    // Subclasses
    // =============================================================================
}
