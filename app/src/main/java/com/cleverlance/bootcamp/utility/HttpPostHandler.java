package com.cleverlance.bootcamp.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;

import com.cleverlance.bootcamp.common.Constants;
import com.cleverlance.bootcamp.common.Enums;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostHandler extends AsyncTask<Void, Void, Void>{
    // =============================================================================
    // Fields
    // =============================================================================

    String username;
    String encryptedPassword;
    OnHttpPostResponse delegate;

    Bitmap decodedBitmap;
    Enums.postResponseStatus status = Enums.postResponseStatus.OK;

    // =============================================================================
    // Constructors
    // =============================================================================

    public HttpPostHandler(OnHttpPostResponse delegate, String username, String password){
        this.username = username;
        this.encryptedPassword = password;
        this.delegate = delegate;
    }

    // =============================================================================
    // Override Methods
    // =============================================================================

    @Override
    protected Void doInBackground(Void... params) {
        postData();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        delegate.onHttpPostResponse(this.decodedBitmap, status);
    }

    // =============================================================================
    // Methods
    // =============================================================================

    /**
     * Sends the POST request and also handles the response
     * sets "status" on the basis of the response
     */
    private void postData() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://mobility.cleverlance.com/download/bootcamp/image.php");

        try {
            // Header and data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("username", this.username));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            httppost.addHeader("Authorization", this.encryptedPassword);
            httppost.addHeader("Content-Type", "application/x-www-form-urlencoded");

            HttpResponse response = httpclient.execute(httppost);
            String responseBody = EntityUtils.toString(response.getEntity());

            // Handling the response
            try{
                JSONObject json = new JSONObject(responseBody);
                String imageBase64 = (String) json.get(Constants.IMAGE);
                byte[] decodedString = Base64.decode(imageBase64, Base64.DEFAULT);
                this.decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            }
            catch (JSONException e){
                // the response is not a JSON String, therefore I presume, that the credentials are
                // wrong
                status = Enums.postResponseStatus.WRONG_CREDENTIALS;
                e.printStackTrace();
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
            status = Enums.postResponseStatus.CONNECTION_ERROR;
        } catch (IOException e) {
            status = Enums.postResponseStatus.CONNECTION_ERROR;
        }
    }
}
