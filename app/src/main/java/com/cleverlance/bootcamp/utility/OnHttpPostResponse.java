package com.cleverlance.bootcamp.utility;

import android.graphics.Bitmap;

import com.cleverlance.bootcamp.common.Enums;

public interface OnHttpPostResponse {
    void onHttpPostResponse(Bitmap bitmap, Enums.postResponseStatus status);
}
